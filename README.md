# UniProj

Semplice container di progetti sviluppati durante alcuni corsi universitari.
Di seguito la lista di progetti:

- TweetPro: Progetto per il corso di Metodologie di programmazione (Java). Voto:30/30

- CityReport: Progetto per il corso di Interazione Uomo-Macchina. Voto 27/30

- WatchIt: Progetto per il corso di Ingegneria del software. Voto: 28/30 
