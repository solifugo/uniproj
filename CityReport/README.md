# City Report

### Progetto sviluppato durante il corso di Interazione Uomo-Macchina

#### Cos'&egrave; e in cosa consiste
Il progetto aveva come scopo lo sviluppo di un prototipo di un applicazione (topic a scelta dello studente). E' stato necessario quindi studiare i fondamentali della UI/UX e sottoporre interviste e questionari al fine di ricercare le funzionalita' gradite da potenziali utenti.
Voto all'esame: 27/30

L'app permette di rimanere aggiornati in tempo reale sui disagi che si presentano nella propria città. Difatti è possibile **segnalare** o **cercare** notizie 
in base a 3 diversi campi:
- **Mezzi pubblici** (ad esempio linee soppresse, guasti bus, etc)
- **Traffico** (prenseza di traffico di varia intensità)
- **Viabilità** (ad esempio strade chiuse al transito per lavori in corso)

### **Puoi trovare il prototipo dell'app [Qui](https://marvelapp.com/3jj1ddd)**
