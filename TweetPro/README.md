[IT]
Progetto Universitario.
Framework per l’analisi e la gestione dei Tweet della piattaforma Twitter. Realizzazione di un 
algoritmo di tipo Count-Min sui Big Data per l’individuazione degli hashtag più utilizzati 
dagli utenti e di un parser self-made dei tweet da file JSON (formato ufficiale dall’api di 
Twitter). Tecnologie usate: Java.

<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>

[EN]
University Project.
Framework for the analysis and management of the Tweets of the Twitter platform. Realization of a
Count-Min algorithm on Big Data to identify the most used hashtags
by users and a self-made parser of tweets from JSON files (official format from the api of
Twitter). Used technologies: Java