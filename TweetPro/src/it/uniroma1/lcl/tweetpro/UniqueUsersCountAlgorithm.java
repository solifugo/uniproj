package it.uniroma1.lcl.tweetpro;

import java.util.List;

public interface UniqueUsersCountAlgorithm
{
	Integer run(List<User> twl);
}
