package it.uniroma1.lcl.tweetpro;

import java.util.List;
import java.util.stream.Collectors;

public class DefaultUserCountAlgorithm implements UniqueUsersCountAlgorithm
{

	@Override
	public Integer run(List<User> ul)
	{
		return ul.stream().collect(Collectors.toSet()).size();
	}

}
