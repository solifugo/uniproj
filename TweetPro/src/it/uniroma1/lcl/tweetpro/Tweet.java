package it.uniroma1.lcl.tweetpro;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

public class Tweet 
{
	private long tweetId; //Identificatore del tweet
	private User user; //Utente proprietario del tweet
	private String text; //Testo del Tweet
	private List<String> hashtagLst; //Lista degli hashtag nel tweet
	private int favorite_count; //Numero di like 
	private int nRetweet; //Numero di volte che il tweet e� stato retwittato
	private boolean isRet; //true se il tweet e� un retweet, false altrimenti
	private Optional<URL> media; //Eventuale immagine presente nel Tweet
	
	public Tweet(String data, boolean isRet)
	{	
		this.hashtagLst = new LinkedList<String>();
		this.isRet = isRet;
		this.media = Optional.empty();
		
		//DIVISIONE DEI DATI DEL TWEET DA QUELLI UTENTE
		String[] ds = data.split("\n");
		StringBuilder dData = new StringBuilder();
		int userLimit = 0;
		
		for(int i=0; i<ds.length; i++)
		{
			if(ds[i].startsWith(" user :{"))
			{
				dData.append("----------------------------\n");
				dData.append(ds[i]+"\n");
				userLimit++;
				while(userLimit>0)
				{
					i++;
					if(ds[i].contains("{")) 
					{
						userLimit++;
					}
					else if(ds[i].contains("}"))
					{
						userLimit--;
					}
					dData.append(ds[i]+"\n");
				}
				dData.append("----------------------------\n");
			}
			else dData.append(ds[i]+"\n");
		}
		
		//CREAZIONE DELL'UTENTE
		String[] splitData = dData.toString().split("----------------------------");
		this.user = new User(splitData[1]);
		
		//ELABORAZIONE DATI E CREAZIONE DEL TWEET
		for(String s : splitData[0].split("\n"))
		{
			if(s.startsWith(" id :")) this.tweetId = Long.parseLong(s.substring(s.indexOf(":")+1));
			
			else if(s.startsWith(" text :")) this.text= s.substring(s.indexOf(":")+2);
		}
		
		String[] sd = splitData[2].split("\n");
		for(int i=0; i<sd.length; i++)
		{	
			if(sd[i].startsWith(" retweet_count :")) this.nRetweet = Integer.parseInt(sd[i].substring(sd[i].indexOf(":")+1));
			
			else if (sd[i].startsWith(" favorite_count :")) this.favorite_count = Integer.parseInt(sd[i].substring(sd[i].indexOf(":")+1));
			
			else if (sd[i].startsWith(" hashtags :[{")) 
			{
				hashtagLst.add(sd[i].substring(sd[i].indexOf("{")+8));
				i++;
				
				while (!sd[i].endsWith("}]"))
				{
					if (sd[i].startsWith(" text :")) 
					{
						hashtagLst.add(sd[i].substring(sd[i].indexOf(":")+2));
					}
					i++;
				}
			}
			else if(sd[i].startsWith(" media_url :"))
			{
				try
				{
					String p[] = sd[i].substring(sd[i].indexOf("media_url :")+12).replace('\\', ' ').split(" ");
					
					this.media = Optional.of(new URL(String.join("", p)));
				}
				catch (MalformedURLException e)
				{
					e.printStackTrace();
				}
			}
		}
	}
	
	/**
	 * Restituisce l�id del tweet.
	 * @return tweetId
	 */
	public long getID()
	{
		return tweetId;
	}
	
	/**
	 * Restituisce l�utente che ha scritto il tweet.
	 * @return user
	 */
	public User getUser()
	{
		return user;
	}
	
	/**
	 * Restituisce il testo del tweet.
	 * @return text
	 */
	public String getText()
	{
		return text;
	}
	
	/**
	 * Restituisce una lista di hashtag (senza il canceletto) contenuti nel tweet.
	 * @return Lista di hashtag del Tweet
	 */
	public List<String> getHashtags()
	{
		return hashtagLst;
	}
	
	/**
	 * Restituisce il numero di like
	 * @return favorite_count
	 */
	public int getLikeCount()
	{
		return favorite_count;
	}
	
	/**
	 * Restituisce il numero di retweet.
	 * @return nRetweet
	 */
	public int getRTCount()
	{
		return nRetweet;
	}
	
	/**
	 * Indica se il tweet e� un retweet.
	 * @return isRet
	 */
	public boolean isRetweet()
	{
		return isRet;
	}
	
	/**
	 * Restituisce il Tweet originale.
	 * @return Tweet
	 */
	public Tweet getOriginalTweet()
	{
		return this;
	}
	
	/**
	 * ritorna il Tweet stesso
	 * @return self
	 */
	public Tweet getSelf()
	{
		return this;
	}
	
	/**
	 * Restituisce un Optional con la URL dell�imagine.
	 * @return media
	 */
	public Optional<URL> getMedia()
	{
		return media;
	}

	@Override
	public int hashCode()
	{
		return 31 * 1 + (int) (tweetId ^ (tweetId >>> 32));
	}

	@Override
	public boolean equals(Object obj)
	{
		if (this == obj) return true;
		
		if (obj == null || getClass() != obj.getClass()) return false;
		
		Tweet other = (Tweet) obj;
		
		if (tweetId != other.tweetId) return false;
		
		return true;
	}
}
