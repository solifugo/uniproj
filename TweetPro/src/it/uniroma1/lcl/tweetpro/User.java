package it.uniroma1.lcl.tweetpro;

public class User 
{
	private long userId; //Identificatore univoco dell�utente.
	private String name; //Nome pubblico dell'utente
	private String screenName; //Nome univoco dell'utente
	private int statuses_count; //Numero di Tweet scritti dall'utente
	private int favourites_count; //Numero di tweet su cui l�utente ha cliccato �mi piace�.
	private int followersN; //Numero di utenti che seguono lo User
	private int followingN; //Numero di altri utenti che l�utente segue.
	private boolean verified; //Indica se l'utente � stato verificato.
	
	public User(String d)
	{
		for(String s : d.split("\n"))
		{
			if(s.startsWith(" id :")) this.userId = Long.parseLong(s.substring(s.indexOf(":")+1));
			
			else if(s.startsWith(" name : ")) this.name = s.substring(s.indexOf(":")+2).trim();
			
			else if(s.startsWith(" screen_name :")) this.screenName = s.substring(s.indexOf(":")+2).trim();
			
			else if(s.startsWith(" statuses_count :")) this.statuses_count = Integer.parseInt(s.substring(s.indexOf(":")+1));
			
			else if(s.startsWith(" favourites_count :")) this.favourites_count = Integer.parseInt(s.substring(s.indexOf(":")+1));
			
			else if(s.startsWith(" followers_count :")) this.followersN = Integer.parseInt(s.substring(s.indexOf(":")+1));
			
			else if(s.startsWith(" friends_count :")) this.followingN = Integer.parseInt(s.substring(s.indexOf(":")+1));
			
			else if(s.startsWith(" verified :"))
			{
				if (s.charAt(s.indexOf(":")+1)=='t') this.verified = true;
			}
		}
	}
	
	/**
	 * Restituisce il nome pubblico dell�utente.
	 * @return name
	 */
	public String getName()
	{
		return name;
	}
	
	/**
	 * Restituisce il nome univoco dell�utente.
	 * @return screenName
	 */
	public String getScreenName()
	{
		return screenName;
	}
	
	/**
	 * Restituisce il numero di tweet che ha scritto l�utente
	 * @return statuses_count
	 */
	public int getTweetsCount()
	{
		return statuses_count;
	}
	
	/**
	 * Restituisce il numero di tweet su cui l�utente ha cliccato �mi piace�.
	 * @return favourites_count
	 */
	public int getFavsCount()
	{
		return favourites_count;
	}
	
	/**
	 * Restituisce il numero di utenti che seguono lo User.
	 * @return followersN
	 */
	public int getFollowersCount()
	{
		return followersN;
	}
	
	/**
	 * Restituisce il numero di altri utenti che l�utente segue.
	 * @return followingN
	 */
	public int getFriendsCount()
	{
		return followingN;
	}
	
	/**
	 * Restituisce l�id dell�utente.
	 * @return userId
	 */
	public long getId()
	{
		return userId;
	}
	
	/**
	 * Indica se l�utente e� stato verificato.
	 * @return verified
	 */
	public boolean isVerified()
	{
		return verified;
	}

	@Override
	public int hashCode()
	{
		return 31 * 1 + (int) (userId ^ (userId >>> 32));
	}

	@Override
	public boolean equals(Object obj)
	{
		if (this == obj) return true;
		
		if (obj == null || getClass() != obj.getClass() ) return false;
		
		User other = (User) obj;
		
		if (userId != other.userId) return false;
		
		return true;
	}

}
