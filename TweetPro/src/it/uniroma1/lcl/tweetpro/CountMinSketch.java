package it.uniroma1.lcl.tweetpro;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;

public class CountMinSketch implements TopHashtagsAlgorithm
{

	@Override
	public List<String> run(List<String> hashtagLst)
	{
		Map<String,Integer> topHashtag = new TreeMap<String, Integer>();
		List<String> result = new LinkedList<String>();
		for(String h : hashtagLst)
		{
			if (topHashtag.containsKey(h)) topHashtag.put(h, topHashtag.get(h)+1) ;
			else topHashtag.put(h, 1);
		}
		
		result.addAll(topHashtag.keySet().stream().sorted((b,a)-> topHashtag.get(a).compareTo(topHashtag.get(b))).collect(Collectors.toList()));
		return result;
	}

}
