package it.uniroma1.lcl.tweetpro;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class TweetCorpus implements Iterable<Tweet> 
{
	private TopHashtagsAlgorithm hashtagAlgorithm; //Algoritmo usato per ricercare i top hashtag
	private UniqueUsersCountAlgorithm userAlgorithm; //Algoritmo usato per contare il numero di utenti unici nel corpus
	private int tweetCount; //Numero di tweet contenuti nel corpus
	private List<Tweet> twl; // Lista di tutti i tweet del corpus
	private Map<User,List<Tweet>> userDatabase; // Le chiavi sono gli utenti unici presenti nel corpus e i valori sono i rispettivi tweet
	private List<String> hashtagsCorpus; // Tutti gli hashtag presenti nei tweet del corpus (Con ripetizioni)
	
	/**
	 * Restitusce un TweetCorpus partendo da un Path di input in cui ciascuna riga e� un singolo tweet 
	 * e il formato della riga segue il JSON standard di Twitter (tuttavia, senza caratteri di a capo).
	 * @param file
	 * @return TweetCorpus
	 * @throws IOException 
	 */
	public static TweetCorpus parseFile(File file) throws IOException
	{
		List<String> corpusSupp = new LinkedList<String>();
		List<String> data = new LinkedList<String>();
		BufferedReader br = new BufferedReader(new FileReader(file));
		
		//ESTRAZIONE DEI DATI
		while(br.ready())
		{
			data.add(br.readLine());
		}
		
		br.close();
	
		//ELABORAZIONE E ORDINAMENTO DEI DATI
		data = data.stream().filter(s-> s.length()>0).map(s-> s.replace('"', ' ')).collect(Collectors.toList());
		
		for(String ds : data)
		{
			String[] dp = ds.split(",");
			StringBuilder supp = new StringBuilder();
			boolean retweeted_area = false;
			int retweeted_limit = 0;
			int ni = 0;
			
			for(int i=0 ; i<dp.length; i++) 
			{
				if(i==ni+1 && i!=0 && retweeted_area==true) supp.append("----------------------------\n");
			
				if(dp[i].startsWith("{"))
				{
					supp.append("{\n"+dp[i].substring(1, dp[i].length()-1)+"\n");
				}
				
				else if(dp[i].contains(":{"))
				{
					int indx = dp[i].indexOf(":{")+2;
					
					if(dp[i].contains("retweeted_status")) 
					{
						ni = i;
						retweeted_area = true;
						retweeted_limit++;
						supp.append("----------------------------\n");
					
						while(retweeted_limit>0)
						{
							ni++;
							
							for(String c : dp[ni].split(""))
							{
								if(c.equals("{")) retweeted_limit++;
								else if(c.equals("}")) retweeted_limit--;
							}
						}
					}
					supp.append(dp[i].substring(0,indx) +"\n"+dp[i].substring(indx)+"\n"); 
				}
				
				else if(dp[i].contains(":[") && dp[i+1].contains("]"))
				{
					supp.append(dp[i]+","+dp[i+1]+"\n");
					i++;
				}
				
				else if(dp[i].contains(":[{"))
				{
					int indx = dp[i].indexOf(":[{");
					supp.append(dp[i].substring(0, indx+2)+"\n{\n"+dp[i].substring(indx+3)+"\n");
				}
				
				else if(dp[i].endsWith("}"))
				{
					supp.append(dp[i].substring(0, dp[i].length()-2)+ "\n},\n");
				}
			
				else supp.append(dp[i]+"\n");
			}
			corpusSupp.add(supp.toString());
		}
		
		return new TweetCorpus(corpusSupp);
	}
	
	private TweetCorpus(List<String> data)
	{
		twl = new LinkedList<Tweet>();
		hashtagsCorpus = new LinkedList<String>();
		
		for(String t : data)
		{
			String[] dataSp;
			
			if(t.contains("retweeted_status")) 
			{
				dataSp = t.split("----------------------------");
				twl.add(new Retweet(dataSp));
			}
			else
			{
				twl.add(new Tweet(t,false));
			}
		}
		tweetCount = twl.size();
		
		//RACCOLTA E ELABORAZIONE DI TUTTI GLI HASHTAG PRESENTI NEL CORPUS
		for(Tweet t : twl)
		{ 
			if(t.getHashtags().size()>0) 
			{
				hashtagsCorpus.addAll(t.getHashtags().stream().map(String::toLowerCase).map(String::trim).collect(Collectors.toList()));
			}
		}
		
		//IMPOSTAZIONE DEGLI ALGORITMI PER I TOP HASHTAG E GLI UTENTI UNICI IN FASE DI COSTRUZIONE 
		setTopHashtagsStrategy();
		setUniqueUsersCountStrategy();
	}
	
	/**
	 * Restituisce il numero di tweet contenuti nel corpus
	 * @return tweetCount
	 */
	public int getTweetCount()
	{
		return tweetCount;
	}
	
	/**
	 * Restituisce i tweet del corpus postati dall�utente specificato
	 * @param user
	 * @return lista di tweet scritti da un utente
	 */
	public List<Tweet> getTweets(User user)
	{
		return userDatabase.get(user);
	}
	
	/**
	 * Restituisce i k hashtag pi� frequenti contenuti nel corpus 
	 * secondo la strategia impostata nel metodo seguente.
	 * @param k
	 * @return topHashtag
	 */
	public List<String> getTopHashtags(int k)
	{	
		if(hashtagsCorpus.isEmpty()) return hashtagsCorpus;
		return hashtagAlgorithm.run(hashtagsCorpus).stream().limit(k).collect(Collectors.toList());
	}
	
	/**
	 * Imposta l�algoritmo di calcolo dei k hashtag pi� frequenti 
	 * (di default, in fase di costruzione dell�oggetto viene impostato l�algoritmo implementato dallo studente).
	 */
	public void setTopHashtagsStrategy()
	{
		setTopHashtagsStrategy(new CountMinSketch());
	}
	
	/**
	 * Metodo di supporto per setTopHashtagsStrategy()
	 * @param a
	 */
	public void setTopHashtagsStrategy(TopHashtagsAlgorithm a)
	{
		hashtagAlgorithm = a;
	}
	
	/**
	 * Restituisce il numero di utenti unici presenti nel corpus
	 * @return utenti unici nel corpus
	 */
	public int getUniqueUsersCount()
	{
		return userAlgorithm.run(twl.stream().map(t-> t.getUser()).collect(Collectors.toList()));
	}
	
	/**
	 * Imposta l�algoritmo di calcolo del numero di utenti unici presenti nel corpus 
	 * (di default, in fase di costruzione dell�oggetto viene impostato l�algoritmo implementato dallo studente).
	 */
	public void setUniqueUsersCountStrategy()
	{
		setUniqueUsersCountStrategy(new DefaultUserCountAlgorithm());
	}
	
	/**
	 * Metodo di supporto per setUniqueUsersCountStrategy()
	 * @param a
	 */
	public void setUniqueUsersCountStrategy(UniqueUsersCountAlgorithm a)
	{
		this.userAlgorithm = a;
	}

	@Override
	public Iterator<Tweet> iterator()
	{
		return new TweetCorpusIterator();
	}
	
	private class TweetCorpusIterator implements Iterator<Tweet>
	{
		private int it;

		@Override
		public boolean hasNext()
		{
			return it < twl.size();
		}

		@Override
		public Tweet next()
		{
			return hasNext() ? twl.get(it++) : null;
		}
	}

}
