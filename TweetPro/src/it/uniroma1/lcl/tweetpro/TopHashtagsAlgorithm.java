package it.uniroma1.lcl.tweetpro;

import java.util.List;

public interface TopHashtagsAlgorithm
{
	List<String> run(List<String> hashtagLst);
}
