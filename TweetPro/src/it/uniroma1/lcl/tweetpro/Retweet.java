package it.uniroma1.lcl.tweetpro;

public class Retweet extends Tweet
{	
	private Tweet srcTweet; //Riferimento al tweet originale
	
	public Retweet(String[] dataSp)
	{
		super(dataSp[0]+dataSp[2],true);
		
		this.srcTweet = new Tweet(dataSp[1],false);
	}
	
	@Override
	public Tweet getOriginalTweet()
	{
		return srcTweet;
	}

}
